//  Post.swift
//  Network
//  Created by Viktoriia Skvarko


import Foundation
import UIKit
import Alamofire

struct DataNetwork {
    
    var userId: Int
    var id: Int
    var title: String
    
    init?(json: [String: Any]) {
        
        guard
            let userId = json["userId"] as? Int,
            let id = json["id"] as? Int,
            let title = json["title"] as? String
        else {
            return nil
        }
        
        self.userId = userId
        self.id = id
        self.title = title
    }
    
    
    static func getArray(from jsonArray: Any) -> [DataNetwork]? {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        guard let jsonArray = jsonArray as? Array<[String: Any]> else { return nil }
        var dataManyNetwork: [DataNetwork] = []
        
        for jsonObject in jsonArray {
            if let dataNetwork = DataNetwork(json: jsonObject) {
                dataManyNetwork.append(dataNetwork)
                
                let strForTable = "\(dataNetwork.userId).\(dataNetwork.id)" + " " + "\(dataNetwork.title)"
                appDelegate.dataForTableVC.listForTable.append(strForTable)
            }
        }
        print(appDelegate.dataForTableVC.listForTable)
        return dataManyNetwork
    }
}
