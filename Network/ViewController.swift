//  ViewController.swift
//  Network
//  Created by Viktoriia Skvarko


import UIKit
import Alamofire

class ViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var tableOutlet: UITableView!
    
    @IBAction func sendAction(_ sender: UIButton) {
        
        AF.request("https://jsonplaceholder.typicode.com/albums").responseJSON { responseJSON in
            
            switch responseJSON.result {
            case .success(let value):
                guard DataNetwork.getArray(from: value) != nil else { return }
                self.tableOutlet.reloadData()
                
            case .failure(let error):
                print(error)
            }
        }
    }
}


extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appDelegate.dataForTableVC.listForTable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "My Cell", for: indexPath)
        cell.textLabel?.text = appDelegate.dataForTableVC.listForTable[indexPath.row]
        return cell
    }
}
